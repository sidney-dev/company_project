import Vue from 'vue'
import Vuex from 'vuex'

import projects from './modules/projects'

import actions from './actions'

Vue.use(Vuex)

const store = new Vuex.Store({

    state: {
        name: 'Sidney'
    },
    actions: actions,
    modules: {
        projects
    }
})

export default store