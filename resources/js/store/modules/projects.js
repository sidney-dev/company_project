export const state = {
    project: '',
    projectTasks: []
};

export const getters = {

    singleProject: state => {
        return state.project
    },

    projectTasks: state => {
        return state.projectTasks
    }
};

export const mutations = {

    LOAD_PROJECT(state, project){
        state.project = project
    },

    LOAD_PROJECT_TASKS(state, tasks){
        state.projectTasks.push(...tasks)
    },

    ADD_TASK(state, task){
        state.projectTasks.unshift(task)
    }
};

export const actions = {

    loadProject({commit}, id){

        axios.get('/api/project/' + id)
        .then(res => {
            commit('LOAD_PROJECT', res.data.project)
            commit('LOAD_PROJECT_TASKS', res.data.tasks)
        })
        .catch( err => console.log(err))
    },

    addProjectTask({commit}, taskData) {
        return new Promise((resolve, reject) => {
            axios.post('/task/project/' + taskData.projectId + '/save', taskData.formData)
            .then(res => {
                commit('ADD_TASK', res.data)
                resolve()
            })
            .catch(err => console.log(err))
        })
        

    }

};



export default {
    state,
    getters,
    mutations,
    actions
}