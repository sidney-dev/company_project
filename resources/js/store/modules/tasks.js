export const state = {
    tasks: [
        { id: 1, name: 'website proposal' },
        { id: 2, name: 'website proposal' }
    ]
};

export const getters = {

    taskList: state => {
        return state.tasks
    }
};

export const mutations = {


};

export const actions = {


};

export default {
    state,
    getters,
    mutations,
    actions
}