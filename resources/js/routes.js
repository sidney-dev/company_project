import Home from './components/Home.vue'
import About from './components/About.vue'
import File from './components/File.vue'
import Project from './components/projects/Main.vue'
import SingleProject from './components/projects/Project.vue'
import ProjectList from './components/projects/ProjectList.vue'
import NewProject from './components/projects/NewProject.vue'
import ProjectEdit from './components/projects/ProjectEdit.vue'

const routes = [

    { path: '/', component: Home },
    { path: '/about', component: About },
    { path: '/file', component: File },

    { path: '/project', component: Project, name: NewProject,
        children: [
            { path: 'list', component: ProjectList, name: 'PojectList' },
            { path: 'add', component: NewProject, name: 'NewProject' },
            { path: ':id', component: SingleProject, name: 'SingleProject' },
            { path: ':id/edit', component: ProjectEdit, name: 'ProjectEdit' }
        ]
    },
    
    { path: '*', redirect: '/' }

]

export default routes