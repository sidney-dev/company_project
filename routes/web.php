<?php

use App\User;

Route::prefix('api')->group(function(){
   // Route::resource('projects', 'ProjectController');

    Route::get('projects', 'ProjectController@index'); // list of ptojects
    Route::get('project/{project}/download', 'ProjectController@downloadFile'); // PDF download
    Route::post('project/save', 'ProjectController@store'); // saving a project
    Route::get('project/{project}', 'ProjectController@show'); // showing a single project

    

    Route::get('task/{task}', 'TaskController@viewProjectTask');

    Route::put('task/project/{project}/update', 'TaskController@updateProjectTask');

    Route::delete('task/{task}/delete', 'TaskController@deleteProjectTask');
    

}); 


Route::post('task/project/{project}/save', 'TaskController@createProjectTask'); // find a project and add a new task to it


Route::get('/{any}', 'SpaController@index')->where('any', '.*');