<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->truncate();
        DB::table('tasks')->truncate();
        DB::table('users')->truncate();
        DB::table('clients')->truncate();
        
        factory(App\Project::class, 10)->create();
        factory(App\Task::class, 20)->create();
        factory(App\User::class, 10)->create();
        factory(App\Client::class, 10)->create();
        // $this->call(UsersTableSeeder::class);
    }
}
