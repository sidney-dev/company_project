<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'description' => $faker->paragraph,
        'summary' => $faker->sentence,
        'owner_id' => $faker->numberBetween(1, 5),
        'client_id' => $faker->numberBetween(1, 5),
        'duration' => $faker->numberBetween(10, 20),
        'status' => $faker->randomElement(['pending', 'completed', 'ongoing'])
    ];
});
