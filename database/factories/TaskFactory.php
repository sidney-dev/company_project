<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'project_id' => $faker->numberBetween(1, 10),
        'name' => $faker->sentence,
        'description' => $faker->paragraph,
        'summary' => $faker->sentence,
        'owner_id' => $faker->numberBetween(1, 10),
        'duration' => $faker->numberBetween(10, 20),
        'tracked_time' => $faker->numberBetween(10, 20)
    ];
});
