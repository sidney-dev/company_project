<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProjectController extends Controller {

    public function index() {
        
        $projects = Project::with('tasks')->withCount('tasks')->orderBy('id', 'desc')->get();

        return $projects;
    }

    public function store(Request $request) {
        
        $input = $request->all();

        if(empty($input['file'])){

            $project = Project::create($request->all());

            return $project;

        } else {
            $project = Project::create([
                'owner_id' => 2,
                'client_id' => 1,
                'name' => $request->name,
                'description' => $request->description,
                'summary' => $request->summary,
                'status' => $request->status,
                'duration' => $request->duration,
            ]);

            $fileName = $request->file->getClientOriginalName();
            $request->file->move(public_path("projects/{$project->id}"), $fileName);

            $project->fill(['file' => $fileName]);

            return $project;

        }

    }


    public function show( Project $project ) {

       /**
        * The line below will show all projects as long as they have tasks
        * return Project::whereHas('tasks')->whereId($project->id)->withCount('tasks')->with('tasks')->get();
        * 
        */


        $project = Project::whereId($project->id)->withCount('tasks')->with('owner')->first();

        $tasks = Task::whereProjectId($project->id)->orderBy('id', 'desc')->get();

        return response()->json([
            'project' => $project,
            'tasks' => $tasks
        ]);


    }

    public function update(Request $request, Project $project){
        
    }

  
    public function destroy(Project $project){
        
    }

    public function downloadFile(Project $project){

        return response()->download(public_path() . "/projects/{$project->id}/{$project->file}");

    }
}
