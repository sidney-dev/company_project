<?php

namespace App\Http\Controllers;

use App\Task;
use App\Project;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function createProjectTask(Request $request, Project $project) {
        $task = Task::create($request->except(['file']));

        if($request->hasFile('file')){
            $fileName = $request->file->getClientOriginalName();
            $request->file->move(public_path("uploads/tasks/{$task->id}"), $fileName);
            $task->fill(['file' => $fileName]);
        }
        return $task;
    }

    /**
     * View a specific task
     */

    public function viewProjectTask(Task $task) {

        return Task::whereId($task->id)->with('owner')->first();

    }

    /**
     * Update a task by its id
     */
    public function updateProjectTask(Request $request, Project $project) {

        return $project->tasks()->update([
            'name' => 'Another sidney task',
        ]);

    }
    

    /**
     * Delete a task
     * 
     * This can be a get method as well
     */
    public function deleteProjectTask(Task $task){
        
        return Task::whereId($task->id)->delete();

        // return "deleted";

    }
}
